<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryProductTable222 extends Migration

{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('category_product', function (Blueprint $table) {
//            $table->increments('id');
//
//            $table->integer('category_id')->unsigned()->default(1);
//            //$table->foreign('category_id')->references('id')->on('categories');
//
//            $table->integer('product_id')->unsigned()->default(1);
//           // $table->foreign('product_id')->references('id')->on('pruducts');
//
//            $table->timestamps();
//        });

        Schema::table('category_product', function(Blueprint $table) {
            $table->foreign('product_id')->references('id')->on('products');
            //$table->foreign('category_id')->references('id')->on('categories');
        });






    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_product');
    }
}