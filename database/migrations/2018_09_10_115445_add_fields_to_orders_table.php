<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
            $table->integer('status')->default(0);
            $table->integer('payment')->default(0);
            $table->integer('delivery')->default(0);
            $table->string('name', 100);
            $table->string('phone', 100);
            $table->integer('user_id')->default(0);
            $table->string('email', 100);
            $table->string('address', 100);
            $table->text('comment');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
            $table->dropColumn('status');
            $table->dropColumn('payment');
            $table->dropColumn('delivery');
            $table->dropColumn('name');
            $table->dropColumn('phone');
            $table->dropColumn('user_id');
            $table->dropColumn('email');
            $table->dropColumn('address');
            $table->dropColumn('comment');
        });
    }
}
