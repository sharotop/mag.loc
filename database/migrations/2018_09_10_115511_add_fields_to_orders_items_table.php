<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToOrdersItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders_items', function (Blueprint $table) {
            //

            $table->integer('order_id');
            $table->integer('catalog_id');
            $table->integer('size_id')->default(0);
            $table->integer('cost');
            $table->integer('count');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders_items', function (Blueprint $table) {
            //
            $table->dropColumn('order_id');
            $table->dropColumn('catalog_id');
            $table->dropColumn('size_id');
            $table->dropColumn('cost');
            $table->dropColumn('count');
        });
    }
}
