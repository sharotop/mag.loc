<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders_item extends Model
{
    //
    protected $fillable = [
        'order_id', 'cost', 'count'
    ];


    public function product(){
        return $this->hasOne('App\Product', 'id' , 'catalog_id');
    }


}
