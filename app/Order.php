<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    protected $fillable = [
        'name', 'email', 'phone'
    ];

//    public function order_items{
//        return $this->hasMany('App\order_items')
//    }
}
