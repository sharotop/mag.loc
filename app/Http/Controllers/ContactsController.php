<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContactsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function contactsForm()
    {
        return view('contacts');
    }


    public function index()
    {
        return view('home');
    }

    public function sendContactsForm(Request $request){


        $name = $request->input('name');
        $email = $request->input('email');
        $subject = $request->input('subject');
        $message = $request->input('message');


        $newQueryArray=array("name"=>$name, "email"=>$email, "subject"=>$subject, "message"=>$message);

        $created=DB::table('messages')->insert($newQueryArray);

        if($created){
            return redirect('contacts')->with('message', 'Ваше сообщение успешно отправлено!');
        }else{
            return redirect('contacts')->with('message', 'Не удалось отправить сообщение!');
        }


    }



}
