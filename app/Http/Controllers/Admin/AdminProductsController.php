<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\CategoriesController;
use App\Message;
use App\Orders_item;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\User;
use App\Order;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class AdminProductsController extends Controller
{
    //

    // все товары
    public function index(){
        $products = Product::paginate(3);
        return view("Admin.displayProducts",['products'=>$products]);

    }

    //Отображение формы добавления товара
    public function createProductsForm(){

        $cat = Category::all()->toArray();
        $categories_for_list = Category::updArr($cat);
        $categories_list_id = Category::categories_list_id($categories_for_list);

        return view("admin.createProductForm")->with([
            'categories'=>$categories_list_id
        ]);
    }

    //отображение формы редактирования товара
    public function editProductsForm($id){
        $product=Product::find($id);
        $cat = Category::all()->toArray();
        $categories_for_list = Category::updArr($cat);
        $categories_list_id = Category::categories_list_id($categories_for_list);

        // получить массив id категорий товаров к которым относится данный товар
        $categories_for_product_edit = Category::categories_for_product_edit($id);

        return view("admin.editProductForm")->with([
            'product'=>$product,
            'categories'=>$categories_list_id,
            'categories_for_product_edit'=>$categories_for_product_edit
        ]);

    }


    public function updateProduct(Request $request,$id){

        // добавляю возможность отредактировать категории и изображение в одной форме.....

        $name = $request->input('name');
        $description = $request->input('description');
        $price = $request->input('price');
        $type = $request->input('type');

        $categories = $request->input('categories');

        $updateArray=array("name"=>$name, "description"=>$description, "type"=>$type, "price"=>$price);

        $product = Product::find($id);
        $product ->update($updateArray);
        $product->categories()->sync($categories);

        return redirect()->route("adminDisplayProducts");

    }

    public function editProductsImageForm($id){
        $product=Product::find($id);
        return view('admin.editProductImageForm',['product'=>$product]);
    }

    public function updateProductImage(Request $request,$id){


        Validator::make($request->all(),['image'=>"required|image|mimes:jpg,png,jpeg|max:5000"])->validate();
        if($request->hasFile("image")){
            $product = Product::find($id);
            $exists = Storage::disk('local')->exists("public/product_images/".$product->image);
            //Если изображение с таким именем уже существует
            if ($exists){
                //Удаляет изображение
                Storage::delete('public/product_images/'.$product->image);
            }
            //Добавляет новое изображение с таким же именем
            $ext = $request->file('image')->getClientOriginalExtension();
            $request->image->storeAs("public/product_images/",$product->image);

            $arrayToUpdate=array('image'=>$product->image);
            DB::table('products')->where('id',$id)->update($arrayToUpdate);

            return redirect()->route("adminDisplayProducts");

        }else{
            $error = "Не выбрано изображение";
            return $error;
        }

    }

    //Отправка формы добавления товара
    public function sendCreateProductForm(Request $request){

        $name = $request->input('name');
        $description = $request->input('description');
        $price = $request->input('price');
        $type = $request->input('type');

        $categories = $request->input('categories');

        //Validator::make($request->all(),['image'=>"required|image|mimes:jpg,png,jpeg|max:5000"])->validate();
        $ext = $request->file('image')->getClientOriginalExtension();

        $imageEncoded = File::get($request->image);

        $ext = $ext ? '.' . $ext : '';
        $path = Storage::url('public/product_images/');
        do {
            $imgName = md5(microtime() . rand(0, 9999));
            $file = $path . $imgName . $ext;
        } while (file_exists($file));

        Storage::disk('local')->put('public/product_images/'.$imgName.$ext,$imageEncoded);

        $newProductArray=array("name"=>$name, "description"=>$description, "type"=>$type, "image"=>$imgName.$ext, "price"=>$price);

        // $created=DB::table('products')->insert($newProductArray);
        $product = Product::create($newProductArray);
        $product->categories()->attach($categories);

        if($product){
            return redirect()->route("adminDisplayProducts");
        }else{
            return "Товар не удалосьдобавить";
        }
    }

    public function deleteProduct($id){

        $product=Product::find($id);

        $exists = Storage::disk('local')->exists("public/product_images/".$product->image);
            //Если изображение с таким именем уже существует
        if ($exists){
            //Удаляет изображение
            Storage::delete('public/product_images/'.$product->image);
        }

        Product::destroy($id);

        return redirect()->route("adminDisplayProducts");
    }

    public function messageShow(){
        $massages = Message::paginate(3);
        return view("Admin.displayMessages",['massages'=>$massages]);
    }

    public function usersShow(){
        $users = User::paginate(3);
        return view("Admin.displayUsers",['users'=>$users]);
    }

    public function ordersShow(){
        $orders = Order::paginate(3);
        return view("Admin.displayOrders",['orders'=>$orders]);
    }

    public function ordersItemsShow($id){
        $orders_items = Orders_item::all()->where('order_id',"==",$id);
        return view("Admin.displayOrdersItems",['orders_items'=>$orders_items]);
    }

    public function search(Request $request){

        $searchText = $request->get('searchText');
        $products = Product::where('name', "Like",$searchText."%")->paginate(3);
        return view("Admin.displayProducts",['products'=>$products]);

    }

}
