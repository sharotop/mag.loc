<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CheckoutController extends Controller
{
    //
    public function __construct()
    {
         $this->middleware('auth');
    }


    public function showСheckoutPage()
    {
        return view('checkout');
    }

    public function sendСheckoutform(Request $request){

        $address = $request->input('address');
        $phone = $request->input('phone');
        $comment = $request->input('comment');
        $name = Auth::user()->name;
        $email = Auth::user()->email;

        $orderQueryArray=array("name"=>$name, "email"=>$email, "address"=>$address, "phone"=>$phone, "comment"=>$comment);

        //Создаем заказ и получаем его id
        $id=DB::table('orders')->insertGetId($orderQueryArray);

        $cart = Session::get('cart');

        //Создаем содержимое заказа
        foreach ($cart->items as $item)
            {
                $orderItemsQueryArray=array("order_id"=>$id, "catalog_id"=>$item['data']['id'], "cost"=>$item['data']['price'], "count"=>$item['quantity']);
                DB::table('orders_items')->insert($orderItemsQueryArray);
            }

          //удаляем все содержимое корзины
          $request->session()->forget("cart");


          //Послать письмо администратору магазина

        if($id){
            return redirect('products')->with('message', 'Ваш заказ принят!');
        }else{
            return redirect('cart')->with('message', 'Не удалось отправить заказ!');
        }

    }
}
