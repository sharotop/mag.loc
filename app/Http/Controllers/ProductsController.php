<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ProductsController extends Controller
{
    //
//    public function index(){
//
//        $products = Product::paginate(3);
//            return view("allproducts",compact("products"));
//    }


//    public function main(){
//
//        $cat = Category::all()->toArray();
//        $categories = Category::updArr($cat);
//        $categories_tree = Category::map_tree($categories);
//
//        $products = Product::paginate(3);
//
//          return view("allproducts")->with([
//            'products'=>$products,
//            'categories_tree'=>$categories_tree,
//            ]);
//
//    }


//    public function search(Request $request){
//
//        $searchText = $request->get('searchText');
//
//        $cat = Category::all()->toArray();
//        $categories = Category::updArr($cat);
//        $categories_tree = Category::map_tree($categories);
//
//        $products = Product::where('name', "Like",$searchText."%")->paginate(3);
//
//        return view("allproducts")->with([
//            'products'=>$products,
//            'categories_tree'=>$categories_tree,
//        ]);
//
//    }





    public function main(Request $request){

        $cat = Category::all()->toArray();
        $categories = Category::updArr($cat);
        $categories_tree = Category::map_tree($categories);

        $brands=Product::getBrand();

        $searchText = $request->get('searchText');
        if ($searchText) {
            $products = Product::where('name', "Like",$searchText."%")->paginate(3);
        }else{
            $products = Product::paginate(3);
        }

        return view("allproducts")->with([
            'products'=>$products,
            'categories_tree'=>$categories_tree,
            'brands'=>$brands,
        ]);

    }


    public function brendShow($alias){

        $cat = Category::all()->toArray();
        $categories = Category::updArr($cat);
        $categories_tree = Category::map_tree($categories);

        $brands=Product::getBrand();

        $products = Product::where('type', "Like",$alias."%")->paginate(3);

        return view("allproducts")->with([
            'products'=>$products,
            'categories_tree'=>$categories_tree,
            'brands'=>$brands,
        ]);


    }


    public function productShow($id){

        $product = Product::find($id);
        $cat = Category::all()->toArray();
        $categories = Category::updArr($cat);
        $categories_tree = Category::map_tree($categories);
        $brands=Product::getBrand();

        return view("product")->with([
            'product'=>$product,
            'categories_tree'=>$categories_tree,
            'brands'=>$brands,
        ]);

    }



    public function addProductToCart(Request $request,$id){

//    $request->session()->forget("cart");
//    $request->session()->flush();

    $prevCart =  $request->session()->get('cart');
    $cart = new Cart($prevCart);

    $product=Product::find($id);
    $cart->addItem($id,$product);
    $request->session()->put('cart', $cart);

    return redirect()->route("allProducts")->with('cartMessage', 'Товар добавлен в корзину!');
}

    public function showCart(){

        $cart = Session::get('cart');

        //корзина не пуста
        if ($cart && $cart->totalQuantity>0){
            return view('cartproducts',['cartItems'=>$cart]);
            //корзина пуста
        }else{
            return redirect()->route("allProducts");
        }

    }

    public function deleteItemFromCart(Request $request, $id){

    $cart = $request->session()->get("cart");

    if (array_key_exists($id,$cart->items)){
        unset($cart->items[$id]);
    }

    $prevCart = $request->session()->get("cart");
    $updateCart = new Cart($prevCart);
    $updateCart->updatePriceAndQuantity();

    $request->session()->put("cart",$updateCart);

    return redirect()->route('cartproducts');

    }


    public function plusProductCart(Request $request, $id){

        $prevCart =  $request->session()->get('cart');
        $cart = new Cart($prevCart);

        $product=Product::find($id);
        $cart->addItem($id,$product);
        $request->session()->put('cart', $cart);

        return redirect()->route("cartproducts");
    }


    public function minusProductCart(Request $request, $id){

        $prevCart =  $request->session()->get('cart');
        $cart = new Cart($prevCart);

        $product=Product::find($id);

        if ($cart->items[$id]['quantity']==1)
                {
                    return redirect()->route("cartproducts");
                }

        $cart->remItem($id,$product);
        $request->session()->put('cart', $cart);

        return redirect()->route("cartproducts");
    }


    public function showСheckoutPage(){

        $cart = Session::get('cart');

        //корзина не пуста
        if ($cart && $cart->totalQuantity>0){
            return view('cartproducts',['cartItems'=>$cart]);
            //корзина пуста
        }else{
            return redirect()->route("allProducts");
        }

    }




}
