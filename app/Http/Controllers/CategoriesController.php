<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

class CategoriesController extends Controller
{
    //
    public function index($id){

        $cat = Category::all()->toArray();
        $categories = Category::updArr($cat);
        $categories_tree = Category::map_tree($categories);

        //вернуть все подкатегории в массиве и по этому массиву найти все id продуктов
        $categories_id = Category::categories_id($categories, $id);
        $categories_id = !$categories_id ? $id : rtrim($categories_id, ",");
        $explode_id = array_map('intval', explode(',', $categories_id));


        $collection = new Collection();

        //$array_ids - содержит id продуктов для вывода
        $array_ids = array();
        $k=0;
        // В $explode_id все id категорий по этому массиву нахожу id нужных продуктов
        for ($i = 0; $i < count($explode_id);$i++){
            //dump(Category::find($explode_id[$i])->products->toArray());
            $array_id = Category::find($explode_id[$i])->products->toArray();
            //dump ($array_id);
            for ($j = 0; $j < count($array_id);$j++){
                //$array_ids=array_push($array_ids, $array_id[$j]['id']);
                $array_ids[$k]=$array_id[$j]['id'];
                $k++;
            }
            $collection=$collection->merge(Category::find($explode_id[$i])->products);
        }

        $products = Product::whereIn('id', $array_ids)->paginate(3);

        return view("allproducts")->with([
            'products'=>$products,
            'categories_tree'=>$categories_tree,
        ]);


    }

}
