<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model
{


    //приводит массив к структуре нужной для построения дерева ф-я map_tree
    public static function updArr($array){
        $arr_cat = array();
        for ($i = 0; $i < count($array);$i++){
            $arr_cat[$array[$i]['id']] = $array[$i];
        }
        return $arr_cat;
    }

    public function products(){
        //return $this->belongsToMany('App\Product','category_product', 'category_id', 'product_id');
        return $this->belongsToMany('App\Product');
    }



    // Построение дерева
    public static function map_tree($dataset) {

        $tree = array();

        foreach ($dataset as $id=>&$node) {
            if (!$node['parent_id']){
                $tree[$id] = &$node;
            }else{
                $dataset[$node['parent_id']]['childs'][$id] = &$node;
            }
        }

        return $tree;
    }




    // Получение дочерних категорий переданного ID
    public static function categories_id($array, $id){
        if(!$id) return false;
        $data="";
        foreach($array as $item){
            if($item['parent_id'] == $id){
                $data .= $item['id'] . ",";
                $data .= self::categories_id($array, $item['id']);
            }
        }
        return $data;
    }


    // Получение ID только дочерних категорий для вывода в форму создания товаров
    public static function categories_list_id($array, $id=0){
        $data=array();
        foreach($array as $obj){
            $flag=0;
                foreach($array as $item) {
                    if ($obj['id'] == $item['parent_id']) {
                        $flag = 1;
                    }
                }
                if ($flag==0) {
                   // dump();
                    $data = array_add($data, $obj['id'], $array[$obj['parent_id']]['title']."-".$obj['title']);
                }
        }

        asort($data);
        reset($data);

        return $data;
    }


    // Получение массива категорий продукта по его id
    public static function categories_for_product_edit($id){
        if(!$id) return false;
        $id *= 1;
        $array = array('0' => $id);
        $data=array();
        $results = DB::select('select category_id from category_product where product_id = ?', $array );

        $i=0;
        foreach($results as $item) {
            $data = array_add($data, $i, $item->category_id);
            $i++;
        }
        return $data;
    }



}
