<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use View;
use Auth;




class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);

        // Используется для доступа к userData из любого шаблона
        View::composer('*',function($view){
            $view->with('userData',Auth::user())->with('userCart',\App\Cart::isCardNotEmpty());
        });

        //Вывод всех sql запросов
//        DB::listen(function ($query) {
//            dump($query->sql);
//        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
