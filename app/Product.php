<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    //
    protected $fillable = [
        'name', 'description', 'image', 'price', 'type'
    ];

    public function getPriceAttribute($value){
//        $newForm = $value."$";
//        return $newForm;
        return $value;
    }

    public function categories(){
        return $this->belongsToMany('App\Category');
    }

    public static function getBrand(){

        $brands = DB::select("select type, COUNT(*) from products group by type ORDER BY count(type) DESC ");

        $new_arr=array();
        $i=0;
        foreach ($brands as $arr){
            $ert=array_values((array)$arr);
            $new_arr[]=$ert;
        }

        return $new_arr;
    }

}
