<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03.09.2018
 * Time: 14:52
 */

namespace App;


use Illuminate\Support\Facades\Session;

class Cart
{
    public $items; // [id'=> ['quantity' => , 'price' =>, 'data' =>], ....]
    public $totalQuantity;
    public $totalPrice;

    /**
     * Cart constructor.
     */
    public function __construct($prevCart)
    {
        if ($prevCart != null){
            $this->items = $prevCart -> items;
            $this->totalQuantity = $prevCart -> totalQuantity;
            $this->totalPrice = $prevCart -> totalPrice;
        } else {
            $this->items = [];
            $this->totalQuantity = 0;
            $this->totalPrice = 0;
        }
    }

    public function  addItem($id,$product){

        $price = $product->price;

        // товар уже добавлен
        if (array_key_exists($id,$this->items)){
            $productToAdd = $this->items[$id];
            $productToAdd['quantity']++;
            $productToAdd['totalSinglePrice']=$productToAdd['quantity'] * $price;

        //товар добавляется в первый раз
        }else{
            $productToAdd = ['quantity'=>1, 'totalSinglePrice'=>$price, 'data'=>$product];
        }

        $this->items[$id]=$productToAdd;
        $this->totalQuantity++;
        $this->totalPrice=$this->totalPrice + $price;
    }


    public function  remItem($id,$product){

        $price = $product->price;

        // товар уже добавлен
        if (array_key_exists($id,$this->items)){
            $productToAdd = $this->items[$id];
            $productToAdd['quantity']--;
            $productToAdd['totalSinglePrice']=$productToAdd['quantity'] * $price;
        }

        $this->items[$id]=$productToAdd;
        $this->totalQuantity--;
        $this->totalPrice=$this->totalPrice - $price;
    }



    public function updatePriceAndQuantity(){

        $totalPrice = 0;
        $totalQuantity = 0;

        foreach($this->items as $item) {
            $totalQuantity = $totalQuantity + $item['quantity'];
            $totalPrice = $totalPrice + $item['totalSinglePrice'];
        }

        $this->totalQuantity = $totalQuantity;
        $this->totalPrice = $totalPrice;

    }

    public static function isCardNotEmpty(){
        $cart = Session::get('cart');
        if ($cart) return $cart->totalQuantity;
    }

}