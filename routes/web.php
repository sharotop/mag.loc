<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Показывает домашнюю страницу
Route::get('/', ["uses"=>"ProductsController@main", "as"=> "main"]);



// Показывает все продукты
Route::get('products', ["uses"=>"ProductsController@main", "as"=> "allProducts"]);

//Поиск
Route::get('search', ["uses"=>"ProductsController@main", "as"=> "searchProducts"]);

// Показывает страницу продукта
Route::get('product/{id}', ["uses"=>"ProductsController@productShow", "as"=> "ProductShow"]);

//вывод бренда
Route::get('brand/{alias}', ["uses"=>"ProductsController@brendShow", "as"=> "BrendShow"]);



//вывод категории
Route::get('category/{id}', ["uses"=>"CategoriesController@index", "as"=> "CategoryShow"]);



// Показывает страницу контактов
Route::get('contacts', ["uses"=>"ContactsController@contactsForm", "as"=> "contacts"]);

// Отправка страницы контактов
Route::post('contacts/send', ["uses"=>"ContactsController@sendContactsForm", "as"=> "sendMassage"]);



// Добавление в корзину
Route::get('product/addToCart/{id}',['uses'=>'ProductsController@addProductToCart','as'=>'AddToCartProduct']);

// Показать содержимое корзины
Route::get('cart', ["uses"=>"ProductsController@showCart", "as"=> "cartproducts"]);

// +1 в корзине
Route::get('cart/plusProduct/{id}', ["uses"=>"ProductsController@plusProductCart", "as"=> "plusProductCart"]);

// -1 в корзине
Route::get('cart/minusProduct/{id}', ["uses"=>"ProductsController@minusProductCart", "as"=> "minusProductCart"]);

// Удаление содержимого корзины
Route::get('product/deleteItemFromCart/{id}',['uses'=>'ProductsController@deleteItemFromCart','as'=>'DeleteItemFromCart']);


// Страница оформления заказа
Route::get('checkout', ["uses"=>"CheckoutController@showСheckoutPage", "as"=> "cartCheckout"]);

// Отправка заказа в базу
Route::post('checkout/send', ["uses"=>"CheckoutController@sendСheckoutform", "as"=> "sendCheckout"]);


//User Authenfication
Auth::routes();


Route::get('/home', 'ContactsController@index')->name('home');


//Панель администратора
Route::get('admin/products', ["uses"=>"Admin\AdminProductsController@index", "as"=> "adminDisplayProducts"])->middleware('restrictToAdmin');

//Показывает форму редактирования изображения товара
Route::get('admin/editProductImageForm/{id}', ["uses"=>"Admin\AdminProductsController@editProductsImageForm", "as"=> "adminEditProductImageForm"])->middleware('restrictToAdmin');

//Показывает форму редактирования товара
Route::get('admin/editProductForm/{id}', ["uses"=>"Admin\AdminProductsController@editProductsForm", "as"=> "adminEditProductForm"])->middleware('restrictToAdmin');

//Обновить изображение у товара
Route::post('admin/updateProductImage/{id}', ["uses"=>"Admin\AdminProductsController@updateProductImage", "as"=> "adminUpdateProductImage"])->middleware('restrictToAdmin');

//Редактирование товара
Route::post('admin/sendEditProductForm/{id}', ["uses"=>"Admin\AdminProductsController@updateProduct", "as"=> "adminUpdateProduct"])->middleware('restrictToAdmin');

//Показывает форму добавления товара
Route::get('admin/createProduct', ["uses"=>"Admin\AdminProductsController@createProductsForm", "as"=> "adminCreateProductForm"])->middleware('restrictToAdmin');

//Добавление товара
Route::post('admin/sendCreateProductForm', ["uses"=>"Admin\AdminProductsController@sendCreateProductForm", "as"=> "adminSendCreateProductForm"])->middleware('restrictToAdmin');

//Удаление товара
Route::get('admin/deleteProduct/{id}', ["uses"=>"Admin\AdminProductsController@deleteProduct", "as"=> "adminDeleteProduct"])->middleware('restrictToAdmin');

//Показывает сообщения
Route::get('admin/messageShow', ["uses"=>"Admin\AdminProductsController@messageShow", "as"=> "adminMessageShow"])->middleware('restrictToAdmin');

//Показывает покупателей
Route::get('admin/usersShow', ["uses"=>"Admin\AdminProductsController@usersShow", "as"=> "adminUsersShow"])->middleware('restrictToAdmin');

//Поиск
Route::get('admin/search', ["uses"=>"Admin\AdminProductsController@search", "as"=> "adminSearchProducts"])->middleware('restrictToAdmin');

//Показывает заказы
Route::get('admin/orders', ["uses"=>"Admin\AdminProductsController@ordersShow", "as"=> "adminOrdersShow"])->middleware('restrictToAdmin');

//Показывает заказы
Route::get('admin/ordersItems/{id}', ["uses"=>"Admin\AdminProductsController@ordersItemsShow", "as"=> "adminOrdersItemsShow"])->middleware('restrictToAdmin');

