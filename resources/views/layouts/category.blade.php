<div class="panel-heading">
    <h4 class="panel-title">
        <li>
            <a href="../category/<?=$items['id']?>"><?=$items['title']?></a>
            @if (array_key_exists('childs',$items))
                <ul>
                    @foreach($items['childs'] as $child)
                        @include('layouts.category', ['items' => $child])
                    @endforeach
                </ul>
            @endif
        </li>
    </span>
    </h4>
</div>









