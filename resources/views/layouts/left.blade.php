
    <div class="col-sm-3">
        <div class="left-sidebar">

            <h2>Категории</h2>

            <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                <div class="panel panel-default">
                    <ul class="category">
                        @foreach($categories_tree as $one)
                            @include('layouts.category', ['items' => $one])
                        @endforeach
                    </ul>
                </div>
            </div>


            <div class="brands_products"><!--brands_products-->
                <h2>Бренды</h2>
                <div class="brands-name">
                    <ul class="nav nav-pills nav-stacked">

                        @foreach($brands as $brand)
                        <li><a href="/brand/{{$brand[0]}}"> <span class="pull-right">({{$brand[1]}})</span>{{$brand[0]}}</a></li>
                        @endforeach



                    </ul>
                </div>
            </div><!--/brands_products-->

            {{--<div class="price-range"><!--price-range-->--}}
                {{--<h2>Диапазон цен</h2>--}}
                {{--<div class="well text-center">--}}
                    {{--<input type="text" class="span2" value="" data-slider-min="0" data-slider-max="600" data-slider-step="5" data-slider-value="[250,450]" id="sl2" ><br />--}}
                    {{--<b class="pull-left">$ 0</b> <b class="pull-right">$ 600</b>--}}
                {{--</div>--}}
            {{--</div><!--/price-range-->--}}

            {{--<div class="shipping text-center"><!--shipping-->--}}
                {{--<img src="{{asset('images/home/shipping.jpg')}}" alt="" />--}}
            {{--</div><!--/shipping-->--}}

        </div>
    </div>
