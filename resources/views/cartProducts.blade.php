@include('layouts.header')


@section('center')

<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="/">Главная</a></li>
                <li class="active">Корзина</li>
            </ol>
        </div>
        <div class="table-responsive cart_info">
            <table class="table table-condensed">
                <thead>

                {{--{{ $userData->name }}--}}

                <tr class="cart_menu">
                    <td class="image">Товар</td>
                    <td class="description"></td>
                    <td class="price">Цена</td>
                    <td class="quantity">Количество</td>
                    <td class="total">Итого</td>
                    <td></td>
                </tr>
                </thead>
                <tbody>


                @foreach ($cartItems->items as $item)

                    <tr>
                        <td class="cart_product">
                            <a href="{{route('ProductShow',['id'=>$item['data']['id']])}}"><img src="{{Storage::disk('local')->url('product_images/'.$item['data']['image'])}}" width="100" height="100" alt=""></a>
                        </td>
                        <td class="cart_description">
                            <h4><a href="{{route('ProductShow',['id'=>$item['data']['id']])}}">{{$item['data']['name']}}</a></h4>
                            <p>{{$item['data']['description']}}</p>
                            <p>{{$item['data']['id']}}</p>

                        </td>
                        <td class="cart_price">
                            <p>{{$item['data']['price']}}</p>
                        </td>
                        <td class="cart_quantity">
                            <div class="cart_quantity_button">
                                <a class="cart_quantity_up" href="{{route('plusProductCart',['id'=>$item['data']['id']])}}"> + </a>
                                <input class="cart_quantity_input" type="text" name="quantity" value="{{$item['quantity']}}" autocomplete="off" size="2">
                                <a class="cart_quantity_down" href="{{route('minusProductCart',['id'=>$item['data']['id']])}}"> - </a>
                            </div>
                        </td>
                        <td class="cart_total">
                            <p class="cart_total_price">{{$item['totalSinglePrice']}}</p>
                        </td>
                        <td class="cart_delete">
                            <a class="cart_quantity_delete" href="{{route('DeleteItemFromCart',['id'=>$item['data']['id']])}}"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>

                @endforeach



                </tbody>
            </table>
        </div>
    </div>
</section> <!--/#cart_items-->

<section id="do_action">
    <div class="container">

        <div class="row">

            <div class="col-sm-6">
                <div class="total_area">
                    <ul>
                        <li>Количество<span>{{$cartItems->totalQuantity}}</span></li>
                        <li>Доставка <span>Free</span></li>
                        <li>Итого к оплате <span>{{$cartItems->totalPrice}}</span></li>
                    </ul>
                    <a class="btn btn-default check_out" href="{{route('cartCheckout')}}">Оформить</a>
                </div>
            </div>
        </div>
    </div>
</section><!--/#do_action-->


@endsection
@yield('center')


@include('layouts.footer')
