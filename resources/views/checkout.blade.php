@extends('layouts.index')

@section('center')




    <section id="cart_items">


        <div class="container">

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="#">Главная</a></li>
                    <li class="active">Оформление</li>
                </ol>
            </div><!--/breadcrums-->

            <div class="shopper-informations">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="shopper-info">
                            <p>Информация о покупателе</p>
                            <p>ФИО: {!! Auth::user()->name !!}</p>
                            <p>email: {!! Auth::user()->email !!}</p>
                        </div>
                    </div>
                    <div class="col-sm-5 clearfix">
                        <div class="bill-to">
                            {{--<p>Дополнительная информация</p>--}}
                            <div class="form-one">
                                {{--<form>--}}
                                <form action="/checkout/send" method="post" id="checkout-form"  name="contact-form">
                                    {{csrf_field()}}
                                    <input type="text" name="address" placeholder="Адрес досавки" required="required">
                                    <input type="text" name="phone" placeholder="Контактный телефон" required="required">
                                    <input type="text" name="comment" placeholder="Комментарий к заказу" required="required">
                                    {{--<a class="btn btn-primary" href="">Оформить</a>--}}
                                    <input type="submit" name="submit" class="btn btn-primary pull-right" value="Оформить">
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>



        </div>



    </section> <!--/#cart_items-->




@endsection