{{--@extends('layouts.index')--}}

{{--@section('slider')--}}
    {{--@if (Request::route()->getName()=="main")--}}
        {{--<section id="slider"><!--slider-->--}}
            {{--<div class="container">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-sm-12">--}}
                        {{--<div id="slider-carousel" class="carousel slide" data-ride="carousel">--}}
                            {{--<ol class="carousel-indicators">--}}
                                {{--<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>--}}
                                {{--<li data-target="#slider-carousel" data-slide-to="1"></li>--}}
                                {{--<li data-target="#slider-carousel" data-slide-to="2"></li>--}}
                            {{--</ol>--}}

                            {{--<div class="carousel-inner">--}}
                                {{--<div class="item active">--}}
                                    {{--<div class="col-sm-6">--}}
                                        {{--<h1><span>E</span>-SHOPPER</h1>--}}
                                        {{--<h2>Акция </h2>--}}
                                        {{--<p>Информация об акции. </p>--}}
                                        {{--<button type="button" class="btn btn-default get">Подробнее</button>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-sm-6">--}}
                                        {{--<img src="{{asset('images/home/girl1.jpg')}}" class="girl img-responsive" alt="" />--}}
                                        {{--<img src="{{asset('images/home/pricing.png')}}"  class="pricing" alt="" />--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="item">--}}
                                    {{--<div class="col-sm-6">--}}
                                        {{--<h1><span>E</span>-SHOPPER</h1>--}}
                                        {{--<h2>100% Уникальный дизайн</h2>--}}
                                        {{--<p>Текст посвященный уникальному дизайну </p>--}}
                                        {{--<button type="button" class="btn btn-default get">Подробнее</button>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-sm-6">--}}
                                        {{--<img src="{{asset('images/home/girl2.jpg')}}" class="girl img-responsive" alt="" />--}}
                                        {{--<img src="{{asset('images/home/pricing.png')}}"  class="pricing" alt="" />--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="item">--}}
                                    {{--<div class="col-sm-6">--}}
                                        {{--<h1><span>E</span>-SHOPPER</h1>--}}
                                        {{--<h2>Скидка</h2>--}}
                                        {{--<p>Дополнительная информация о скидке </p>--}}
                                        {{--<button type="button" class="btn btn-default get">Подробнее</button>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-sm-6">--}}
                                        {{--<img src="{{asset('images/home/girl3.jpg')}}" class="girl img-responsive" alt="" />--}}
                                        {{--<img src="{{asset('images/home/pricing.png')}}" class="pricing" alt="" />--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                            {{--</div>--}}

                            {{--<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">--}}
                                {{--<i class="fa fa-angle-left"></i>--}}
                            {{--</a>--}}
                            {{--<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">--}}
                                {{--<i class="fa fa-angle-right"></i>--}}
                            {{--</a>--}}
                        {{--</div>--}}

                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</section><!--/slider-->--}}
    {{--@endif--}}
{{--@endsection--}}

{{--@section('center')--}}

    {{--<section>--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}

                {{--@include('layouts.left')--}}

                {{--<div class="col-sm-9 padding-right">--}}
                    {{--<div class="features_items"><!--features_items-->--}}
                        {{--<h2 class="title text-center">Товары</h2>--}}

                        {{--@foreach ($products as $product)--}}
                            {{--<div class="col-sm-4">--}}
                                {{--<div class="product-image-wrapper">--}}
                                    {{--<div class="single-products">--}}
                                        {{--<div class="productinfo text-center">--}}
                                            {{--<img  src="{{Storage::disk('local')->url('product_images/'.$product->image)}}" alt=""  />--}}
                                            {{--<h2>{{$product->price}}</h2>--}}
                                            {{--<p>{{$product->name}}</p>--}}
                                            {{--<a href="{{route('AddToCartProduct',['ids'=>$product->id])}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>В корзину</a>--}}
                                        {{--</div>--}}
                                        {{--<div class="product-overlay">--}}
                                            {{--<div class="overlay-content">--}}
                                                {{--<h2>{{$product->price}}</h2>--}}
                                                {{--<p>{{$product->name}}</p>--}}
                                                {{--<a href="{{route('AddToCartProduct',['id'=>$product->id])}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>В корзину</a>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="choose">--}}
                                        {{--<ul class="nav nav-pills nav-justified">--}}
                                            {{--<li><a href="{{route('ProductShow',['id'=>$product->id])}}">Подробнее</a></li>--}}
                                        {{--</ul>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--@endforeach--}}

                        {{--@if ($products->has('links')):--}}
                        {{--{{$products->links()}}--}}
                        {{--@endif--}}

                    {{--</div><!--features_items-->--}}

                    {{--<div class="category-tab"><!--category-tab-->--}}
                        {{--<div class="col-sm-12">--}}
                            {{--<ul class="nav nav-tabs">--}}
                                {{--<li class="active"><a href="#tshirt" data-toggle="tab">T-Shirt</a></li>--}}
                                {{--<li><a href="#blazers" data-toggle="tab">Blazers</a></li>--}}
                                {{--<li><a href="#sunglass" data-toggle="tab">Sunglass</a></li>--}}
                                {{--<li><a href="#kids" data-toggle="tab">Kids</a></li>--}}
                                {{--<li><a href="#poloshirt" data-toggle="tab">Polo shirt</a></li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                        {{--<div class="tab-content">--}}
                            {{--<div class="tab-pane fade active in" id="tshirt" >--}}
                                {{--<div class="col-sm-3">--}}
                                    {{--<div class="product-image-wrapper">--}}
                                        {{--<div class="single-products">--}}
                                            {{--<div class="productinfo text-center">--}}
                                                {{--<img src="{{asset('images/home/gallery1.jpg')}}" alt="" />--}}
                                                {{--<h2>$56</h2>--}}
                                                {{--<p>Easy Polo Black Edition</p>--}}
                                                {{--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
                                            {{--</div>--}}

                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-3">--}}
                                    {{--<div class="product-image-wrapper">--}}
                                        {{--<div class="single-products">--}}
                                            {{--<div class="productinfo text-center">--}}
                                                {{--<img src="{{asset('images/home/gallery2.jpg')}}" alt="" />--}}
                                                {{--<h2>$56</h2>--}}
                                                {{--<p>Easy Polo Black Edition</p>--}}
                                                {{--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
                                            {{--</div>--}}

                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-3">--}}
                                    {{--<div class="product-image-wrapper">--}}
                                        {{--<div class="single-products">--}}
                                            {{--<div class="productinfo text-center">--}}
                                                {{--<img src="{{asset('images/home/gallery3.jpg')}}" alt="" />--}}
                                                {{--<h2>$56</h2>--}}
                                                {{--<p>Easy Polo Black Edition</p>--}}
                                                {{--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
                                            {{--</div>--}}

                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-3">--}}
                                    {{--<div class="product-image-wrapper">--}}
                                        {{--<div class="single-products">--}}
                                            {{--<div class="productinfo text-center">--}}
                                                {{--<img src="{{asset('images/home/gallery4.jpg')}}" alt="" />--}}
                                                {{--<h2>$56</h2>--}}
                                                {{--<p>Easy Polo Black Edition</p>--}}
                                                {{--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
                                            {{--</div>--}}

                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="tab-pane fade" id="blazers" >--}}
                                {{--<div class="col-sm-3">--}}
                                    {{--<div class="product-image-wrapper">--}}
                                        {{--<div class="single-products">--}}
                                            {{--<div class="productinfo text-center">--}}
                                                {{--<img src="{{asset('images/home/gallery4.jpg')}}" alt="" />--}}
                                                {{--<h2>$56</h2>--}}
                                                {{--<p>Easy Polo Black Edition</p>--}}
                                                {{--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
                                            {{--</div>--}}

                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-3">--}}
                                    {{--<div class="product-image-wrapper">--}}
                                        {{--<div class="single-products">--}}
                                            {{--<div class="productinfo text-center">--}}
                                                {{--<img src="{{asset('images/home/gallery3.jpg')}}" alt="" />--}}
                                                {{--<h2>$56</h2>--}}
                                                {{--<p>Easy Polo Black Edition</p>--}}
                                                {{--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
                                            {{--</div>--}}

                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-3">--}}
                                    {{--<div class="product-image-wrapper">--}}
                                        {{--<div class="single-products">--}}
                                            {{--<div class="productinfo text-center">--}}
                                                {{--<img src="{{asset('images/home/gallery2.jpg')}}" alt="" />--}}
                                                {{--<h2>$56</h2>--}}
                                                {{--<p>Easy Polo Black Edition</p>--}}
                                                {{--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
                                            {{--</div>--}}

                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-3">--}}
                                    {{--<div class="product-image-wrapper">--}}
                                        {{--<div class="single-products">--}}
                                            {{--<div class="productinfo text-center">--}}
                                                {{--<img src="{{asset('images/home/gallery1.jpg')}}" alt="" />--}}
                                                {{--<h2>$56</h2>--}}
                                                {{--<p>Easy Polo Black Edition</p>--}}
                                                {{--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
                                            {{--</div>--}}

                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="tab-pane fade" id="sunglass" >--}}
                                {{--<div class="col-sm-3">--}}
                                    {{--<div class="product-image-wrapper">--}}
                                        {{--<div class="single-products">--}}
                                            {{--<div class="productinfo text-center">--}}
                                                {{--<img src="{{asset('images/home/gallery3.jpg')}}" alt="" />--}}
                                                {{--<h2>$56</h2>--}}
                                                {{--<p>Easy Polo Black Edition</p>--}}
                                                {{--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
                                            {{--</div>--}}

                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-3">--}}
                                    {{--<div class="product-image-wrapper">--}}
                                        {{--<div class="single-products">--}}
                                            {{--<div class="productinfo text-center">--}}
                                                {{--<img src="{{asset('images/home/gallery4.jpg')}}" alt="" />--}}
                                                {{--<h2>$56</h2>--}}
                                                {{--<p>Easy Polo Black Edition</p>--}}
                                                {{--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
                                            {{--</div>--}}

                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-3">--}}
                                    {{--<div class="product-image-wrapper">--}}
                                        {{--<div class="single-products">--}}
                                            {{--<div class="productinfo text-center">--}}
                                                {{--<img src="{{asset('images/home/gallery1.jpg')}}" alt="" />--}}
                                                {{--<h2>$56</h2>--}}
                                                {{--<p>Easy Polo Black Edition</p>--}}
                                                {{--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
                                            {{--</div>--}}

                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-3">--}}
                                    {{--<div class="product-image-wrapper">--}}
                                        {{--<div class="single-products">--}}
                                            {{--<div class="productinfo text-center">--}}
                                                {{--<img src="{{asset('images/home/gallery2.jpg')}}" alt="" />--}}
                                                {{--<h2>$56</h2>--}}
                                                {{--<p>Easy Polo Black Edition</p>--}}
                                                {{--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
                                            {{--</div>--}}

                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="tab-pane fade" id="kids" >--}}
                                {{--<div class="col-sm-3">--}}
                                    {{--<div class="product-image-wrapper">--}}
                                        {{--<div class="single-products">--}}
                                            {{--<div class="productinfo text-center">--}}
                                                {{--<img src="{{asset('images/home/gallery1.jpg')}}" alt="" />--}}
                                                {{--<h2>$56</h2>--}}
                                                {{--<p>Easy Polo Black Edition</p>--}}
                                                {{--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
                                            {{--</div>--}}

                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-3">--}}
                                    {{--<div class="product-image-wrapper">--}}
                                        {{--<div class="single-products">--}}
                                            {{--<div class="productinfo text-center">--}}
                                                {{--<img src="{{asset('images/home/gallery2.jpg')}}" alt="" />--}}
                                                {{--<h2>$56</h2>--}}
                                                {{--<p>Easy Polo Black Edition</p>--}}
                                                {{--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
                                            {{--</div>--}}

                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-3">--}}
                                    {{--<div class="product-image-wrapper">--}}
                                        {{--<div class="single-products">--}}
                                            {{--<div class="productinfo text-center">--}}
                                                {{--<img src="{{asset('images/home/gallery3.jpg')}}" alt="" />--}}
                                                {{--<h2>$56</h2>--}}
                                                {{--<p>Easy Polo Black Edition</p>--}}
                                                {{--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
                                            {{--</div>--}}

                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-3">--}}
                                    {{--<div class="product-image-wrapper">--}}
                                        {{--<div class="single-products">--}}
                                            {{--<div class="productinfo text-center">--}}
                                                {{--<img src="{{asset('images/home/gallery4.jpg')}}" alt="" />--}}
                                                {{--<h2>$56</h2>--}}
                                                {{--<p>Easy Polo Black Edition</p>--}}
                                                {{--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
                                            {{--</div>--}}

                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="tab-pane fade" id="poloshirt" >--}}
                                {{--<div class="col-sm-3">--}}
                                    {{--<div class="product-image-wrapper">--}}
                                        {{--<div class="single-products">--}}
                                            {{--<div class="productinfo text-center">--}}
                                                {{--<img src="{{asset('images/home/gallery2.jpg')}}" alt="" />--}}
                                                {{--<h2>$56</h2>--}}
                                                {{--<p>Easy Polo Black Edition</p>--}}
                                                {{--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
                                            {{--</div>--}}

                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-3">--}}
                                    {{--<div class="product-image-wrapper">--}}
                                        {{--<div class="single-products">--}}
                                            {{--<div class="productinfo text-center">--}}
                                                {{--<img src="{{asset('images/home/gallery4.jpg')}}" alt="" />--}}
                                                {{--<h2>$56</h2>--}}
                                                {{--<p>Easy Polo Black Edition</p>--}}
                                                {{--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
                                            {{--</div>--}}

                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-3">--}}
                                    {{--<div class="product-image-wrapper">--}}
                                        {{--<div class="single-products">--}}
                                            {{--<div class="productinfo text-center">--}}
                                                {{--<img src="{{asset('images/home/gallery3.jpg')}}" alt="" />--}}
                                                {{--<h2>$56</h2>--}}
                                                {{--<p>Easy Polo Black Edition</p>--}}
                                                {{--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
                                            {{--</div>--}}

                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-3">--}}
                                    {{--<div class="product-image-wrapper">--}}
                                        {{--<div class="single-products">--}}
                                            {{--<div class="productinfo text-center">--}}
                                                {{--<img src="{{asset('images/home/gallery1.jpg')}}" alt="" />--}}
                                                {{--<h2>$56</h2>--}}
                                                {{--<p>Easy Polo Black Edition</p>--}}
                                                {{--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
                                            {{--</div>--}}

                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div><!--/category-tab-->--}}

                    {{--<div class="recommended_items"><!--recommended_items-->--}}
                        {{--<h2 class="title text-center">recommended items</h2>--}}

                        {{--<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">--}}
                            {{--<div class="carousel-inner">--}}
                                {{--<div class="item active">--}}
                                    {{--<div class="col-sm-4">--}}
                                        {{--<div class="product-image-wrapper">--}}
                                            {{--<div class="single-products">--}}
                                                {{--<div class="productinfo text-center">--}}
                                                    {{--<img src="{{asset('images/home/recommend1.jpg')}}" alt="" />--}}
                                                    {{--<h2>$56</h2>--}}
                                                    {{--<p>Easy Polo Black Edition</p>--}}
                                                    {{--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
                                                {{--</div>--}}

                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-sm-4">--}}
                                        {{--<div class="product-image-wrapper">--}}
                                            {{--<div class="single-products">--}}
                                                {{--<div class="productinfo text-center">--}}
                                                    {{--<img src="{{asset('images/home/recommend2.jpg')}}" alt="" />--}}
                                                    {{--<h2>$56</h2>--}}
                                                    {{--<p>Easy Polo Black Edition</p>--}}
                                                    {{--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
                                                {{--</div>--}}

                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-sm-4">--}}
                                        {{--<div class="product-image-wrapper">--}}
                                            {{--<div class="single-products">--}}
                                                {{--<div class="productinfo text-center">--}}
                                                    {{--<img src="{{asset('images/home/recommend3.jpg')}}" alt="" />--}}
                                                    {{--<h2>$56</h2>--}}
                                                    {{--<p>Easy Polo Black Edition</p>--}}
                                                    {{--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
                                                {{--</div>--}}

                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="item">--}}
                                    {{--<div class="col-sm-4">--}}
                                        {{--<div class="product-image-wrapper">--}}
                                            {{--<div class="single-products">--}}
                                                {{--<div class="productinfo text-center">--}}
                                                    {{--<img src="{{asset('images/home/recommend1.jpg')}}" alt="" />--}}
                                                    {{--<h2>$56</h2>--}}
                                                    {{--<p>Easy Polo Black Edition</p>--}}
                                                    {{--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
                                                {{--</div>--}}

                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-sm-4">--}}
                                        {{--<div class="product-image-wrapper">--}}
                                            {{--<div class="single-products">--}}
                                                {{--<div class="productinfo text-center">--}}
                                                    {{--<img src="{{asset('images/home/recommend2.jpg')}}" alt="" />--}}
                                                    {{--<h2>$56</h2>--}}
                                                    {{--<p>Easy Polo Black Edition</p>--}}
                                                    {{--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
                                                {{--</div>--}}

                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-sm-4">--}}
                                        {{--<div class="product-image-wrapper">--}}
                                            {{--<div class="single-products">--}}
                                                {{--<div class="productinfo text-center">--}}
                                                    {{--<img src="{{asset('images/home/recommend3.jpg')}}" alt="" />--}}
                                                    {{--<h2>$56</h2>--}}
                                                    {{--<p>Easy Polo Black Edition</p>--}}
                                                    {{--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
                                                {{--</div>--}}

                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">--}}
                                {{--<i class="fa fa-angle-left"></i>--}}
                            {{--</a>--}}
                            {{--<a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">--}}
                                {{--<i class="fa fa-angle-right"></i>--}}
                            {{--</a>--}}
                        {{--</div>--}}
                    {{--</div><!--/recommended_items-->--}}

                {{--</div>--}}

            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}

{{--@endsection--}}





