@extends('layouts.admin')

@section('body')

    <div class="table-responsive">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    <li>{!! print_r($errors->all()) !!}</li>
                </ul>

            </div>
        @endif

            <h2>Добавление товара</h2>

        <form action="/admin/sendCreateProductForm" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                <label for="name">Наименование</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Product Name" required>
            </div>
            <div class="form-group">
                <label for="description">Описание</label>
                <input type="text" class="form-control" name="description" id="description" placeholder="description" required>
            </div>
            <div class="form-group">
                <label for="image">Изображение</label>
                <input type="file" class="" name="image" id="image" required>
            </div>
            <div class="form-group">
                <label for="price">Цена</label>
                <input type="text" class="form-control" name="price" id="price" placeholder="price" required>
            </div>
            <div class="form-group">
                <label for="type">Тип</label>
                <input type="text" class="form-control" name="type" id="type" placeholder="type" required>
            </div>

            <div class="form-group">
                <label for="categories">Категории</label>
                    <select class="form-control" name="categories[]" id="categories" multiple required>



                        @foreach($categories as $key=>$val)
                            <option value="{{$key}}">{{$val}}</option>
                        @endforeach




                    </select>
            </div>


            <button type="submit" name="submit" class="btn btn-defautl">Сохранить</button>
        </form>
    </div>

@endsection
