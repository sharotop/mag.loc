@extends('layouts.admin')

@section('body')
    <h2>Товары</h2>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#id</th>
                <th>email</th>
                <th>Тema</th>
                <th>Сообщение</th>
                {{--<th>Уалить</th>--}}
            </tr>
            </thead>
            <tbody>

            @foreach($massages as $massage)
                <tr>
                    <td>{{$massage['id']}}</td>
                    <td>{{$massage['email']}}</td>
                    <td>{{$massage['subject']}}</td>
                    <td>{{$massage['message']}}</td>
                    {{--<td><a href="{{route('adminDeleteProduct',['id' => $product['id']])}}" class="btn btn-primary" >Удалить</a></td>--}}
                </tr>
            @endforeach

            </tbody>
        </table>
        {{$massages->links()}}
    </div>


@endsection