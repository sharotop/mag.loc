@extends('layouts.admin')

@section('body')
    <h2>Товары</h2>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#id</th>
                <th>Имя</th>
                <th>email</th>
            </tr>
            </thead>
            <tbody>

            @foreach($users as $user)
                <tr>
                    <td>{{$user['id']}}</td>
                    <td>{{$user['name']}}</td>
                    <td>{{$user['email']}}</td>
                </tr>
            @endforeach

            </tbody>
        </table>
        {{$users->links()}}
    </div>


@endsection