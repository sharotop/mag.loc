@extends('layouts.admin')

@section('body')
<h2>Товары</h2>
<div class="table-responsive">
    <table class="table table-striped table-sm">
        <thead>
        <tr>
            <th>#id</th>
            <th>Изображение</th>
            <th>Имя</th>
            <th>Описание</th>
            <th>Тип</th>
            <th>Цена</th>
            <th>Редактировать изображение</th>
            <th>Редактировать товар</th>
            <th>Уалить</th>
        </tr>
        </thead>
        <tbody>

        @foreach($products as $product)
        <tr>
            <td>{{$product['id']}}</td>
            <td><img src="{{ Storage::url('product_images/'.$product['image'])}}" alt="{{ Storage::url('product_images/'.$product['image'])}}" width="100" height="100" style="max-height:220px" > </td>
            <td>{{$product['name']}}</td>
            <td>{{$product['description']}}</td>
            <td>{{$product['type']}}</td>
            <td>{{$product['price']}}</td>
            <td><a href="{{ route('adminEditProductImageForm',['id' => $product['id']])}}" class="btn btn-primary" >Редактировать изображение</a></td>
            <td><a href="{{ route('adminEditProductForm',['id' => $product['id']])}}" class="btn btn-primary" >Редактировать товар</a></td>
            <td><a href="{{route('adminDeleteProduct',['id' => $product['id']])}}" class="btn btn-primary" >Удалить</a></td>
        </tr>
        @endforeach

        </tbody>
    </table>
    {{$products->links()}}
</div>


@endsection