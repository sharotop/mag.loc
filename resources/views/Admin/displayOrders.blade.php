@extends('layouts.admin')

@section('body')
    <h2>Заказы</h2>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#id</th>
                <th>email</th>
                <th>Телефон</th>
                <th>Статус</th>
                <th>Содеоржимое</th>
                {{--<th>Уалить</th>--}}
            </tr>
            </thead>
            <tbody>

            @foreach($orders as $order)
                <tr>
                    <td>{{$order['id']}}</td>
                    <td>{{$order['email']}}</td>
                    <td>{{$order['phone']}}</td>
                    <td>{{$order['status']}}</td>
                    <td><a href="{{route('adminOrdersItemsShow',['id' => $order['id']])}}" class="btn btn-primary" >Смотреть</a></td>
                    {{--<td><a href="{{route('adminDeleteProduct',['id' => $product['id']])}}" class="btn btn-primary" >Удалить</a></td>--}}
                </tr>
            @endforeach

            </tbody>
        </table>
        {{$orders->links()}}
    </div>


@endsection