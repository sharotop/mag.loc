@extends ('layouts.admin')

@section('body')

    <div class="table-responsive">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    <li> {!! print_r($errors->all()) !!} </li>
                </ul>
            </div>
        @endif

        <h2>Текущее Изображение</h2>

        <div>
            <img src="{{asset ('storage')}}/{{'product_images/'.$product['image']}}" widht="100" height="100" style="max-heaght:220px" >
        </div>

        <form action="/admin/updateProductImage/{{$product->id}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                {{--<label for="description">Редактировать изображение</label>--}}
                <input type="file" class="" name="image" id="image" placeholder="image" value="{{$product->image}}" required>
            </div>
            <button type="submit" name="submit" class="btn btn-default">Сохранить</button>
        </form>

    </div>
@endsection