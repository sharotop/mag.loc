@extends('layouts.admin')

@section('body')
    <h2>содержимое заказа №{{$orders_items->first()['order_id']}}</h2>
{{----}}
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#id</th>
                <th>Наименование</th>
                <th>Цена</th>
                <th>Количество</th>
                {{--<th>Содеоржимое</th>--}}
                {{--<th>Уалить</th>--}}
            </tr>
            </thead>
            <tbody>

            @foreach($orders_items as $orders_item)
                <tr>
                    <td>{{$orders_item['id']}}</td>
                    <td>{{$orders_item->product['name']}}</td>
                    <td>{{$orders_item['cost']}}</td>
                    <td>{{$orders_item['count']}}</td>
                    {{--<td><a href="" class="btn btn-primary" >Смотреть</a></td>--}}
                    {{--<td><a href="{{route('adminDeleteProduct',['id' => $product['id']])}}" class="btn btn-primary" >Удалить</a></td>--}}
                </tr>
            @endforeach

            </tbody>
        </table>
        {{--{{$orders_items->links()}}--}}
    </div>


@endsection