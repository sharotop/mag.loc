@extends('layouts.admin')

@section('body')



<h2>Редактирование товара</h2>
<div class="table-responsive">
    <form action="/admin/sendEditProductForm/{{$product->id}}" method="post">
        {{csrf_field()}}
        <div class="form-group">
            <label for="name">Наименование</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Product Name" value="{{$product->name}}" required>
        </div>
        <div class="form-group">
            <label for="description">Описание</label>
            <input type="text" class="form-control" name="description" id="description" placeholder="description" value="{{$product->description}}" required>
        </div>
        <div class="form-group">
            <label for="price">Цена</label>
            <input type="text" class="form-control" name="price" id="price" placeholder="price" value="{{$product->price}}" required>
        </div>
        <div class="form-group">
            <label for="price">Тип</label>
            <input type="text" class="form-control" name="type" id="type" placeholder="type" value="{{$product->type}}" required>
        </div>


        <select class="form-control" name="categories[]" id="categories" multiple required>

            @foreach($categories as $key=>$val)
                <option @if (in_array($key, ($categories_for_product_edit))) selected="selected"  @endif  value="{{$key}}">{{$val}}</option>
            @endforeach


        </select>

        <button type="submit" name="submit" class="btn btn-defautl">Сохранить</button>
    </form>
</div>

@endsection
