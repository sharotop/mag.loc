-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Сен 13 2018 г., 13:16
-- Версия сервера: 5.6.38
-- Версия PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `mag_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `parent_id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `created_at`, `updated_at`, `parent_id`, `title`) VALUES
(1, NULL, NULL, 0, 'Одежда'),
(2, NULL, NULL, 0, 'Обувь'),
(3, NULL, NULL, 0, 'Аксесуары'),
(4, NULL, NULL, 1, 'Рубашки'),
(5, NULL, NULL, 1, 'Джинсы'),
(6, NULL, NULL, 4, 'Короткий рукав'),
(7, NULL, NULL, 2, 'Кроссовки'),
(8, NULL, NULL, 2, 'Туфли'),
(9, NULL, NULL, 3, 'Очки'),
(10, NULL, NULL, 3, 'Сумки'),
(11, NULL, NULL, 4, 'Длинный рукав');

-- --------------------------------------------------------

--
-- Структура таблицы `categories_related`
--

CREATE TABLE `categories_related` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `messages`
--

INSERT INTO `messages` (`id`, `created_at`, `updated_at`, `name`, `email`, `subject`, `message`) VALUES
(11, NULL, NULL, '111', 'test2@email.com', '222', '333'),
(12, NULL, NULL, '111', 'test2@email.com', '222', '333'),
(13, NULL, NULL, '111', 'test2@email.com', '222', '333'),
(14, NULL, NULL, '111', 'test2@email.com', '222', '333'),
(15, NULL, NULL, '111', 'test2@email.com', '222', '333'),
(16, NULL, NULL, '111', 'test2@email.com', '222', '333'),
(17, NULL, NULL, '111', 'test2@email.com', '222', '333'),
(18, NULL, NULL, '111', 'test2@email.com', '222', '333'),
(19, NULL, NULL, '111', 'test2@email.com', '222', '333'),
(20, NULL, NULL, '111', 'test2@email.com', '222', '333'),
(21, NULL, NULL, '33', 'Test3@email.com', '44', '55'),
(22, NULL, NULL, 'Тестовое имя', 'test@email.com', 'Тема сообщения', 'Очень длинное сообщение, которое никуда не помещается ... Очень длинное сообщение, которое никуда не помещается ... Очень длинное сообщение, которое никуда не помещается ... Очень длинное сообщение, которое никуда не помещается ... Очень длинное сообщение, которое никуда не помещается ... Очень длинное сообщение, которое никуда не помещается ... Очень длинное сообщение, которое никуда не помещается ... Очень длинное сообщение, которое никуда не помещается ... Очень длинное сообщение, которое никуда не помещается ... Очень длинное сообщение, которое никуда не помещается ... Очень длинное сообщение, которое никуда не помещается ... Очень длинное сообщение, которое никуда не помещается ... Очень длинное сообщение, которое никуда не помещается ... Очень длинное сообщение, которое никуда не помещается ... Очень длинное сообщение, которое никуда не помещается ... Очень длинное сообщение, которое никуда не помещается ... Очень длинное сообщение, которое никуда не помещается ... Очень длинное сообщение, которое никуда не помещается ... Очень длинное сообщение, которое никуда не помещается ... Очень длинное сообщение, которое никуда не помещается ... Очень длинное сообщение, которое никуда не помещается ... Очень длинное сообщение, которое никуда не помещается ... Очень длинное сообщение, которое никуда не помещается ... Очень длинное сообщение, которое никуда не помещается ... Очень длинное сообщение, которое никуда не помещается ...'),
(23, NULL, NULL, '111', 'Email@qqq.rr', 'ee', 'eeee');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_09_03_075606_create_products_table', 1),
(4, '2018_09_03_081015_add_fieldss_to_products_table', 2),
(5, '2018_09_06_080159_add_admin_field', 3),
(6, '2018_09_07_111023_create_messages_table', 4),
(7, '2018_09_07_111202_add_fields_to_messages_table', 4),
(8, '2018_09_10_115227_create_orders_table', 5),
(9, '2018_09_10_115254_create_orders_itmes_table', 5),
(12, '2018_09_10_115445_add_fields_to_orders_table', 6),
(13, '2018_09_10_115511_add_fields_to_orders_items_table', 6),
(14, '2018_09_12_134654_create__categories_table', 7),
(15, '2018_09_12_134854_add_votes_to_categories_table', 7),
(16, '2018_09_12_140445_create_categories_related_table', 8),
(17, '2018_09_12_140544_add_votes_to_categories_related_table', 8);

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `payment` int(11) NOT NULL DEFAULT '0',
  `delivery` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `created_at`, `updated_at`, `status`, `payment`, `delivery`, `name`, `phone`, `user_id`, `email`, `address`, `comment`) VALUES
(14, NULL, NULL, 1, 0, 0, 'TestName', '5', 0, 'Test@email.com', '5', '5'),
(15, NULL, NULL, 0, 0, 0, 'TestName', '3', 0, 'Test@email.com', '3', '3'),
(16, NULL, NULL, 0, 0, 0, 'TestName', '777', 0, 'Test@email.com', '777', '777');

-- --------------------------------------------------------

--
-- Структура таблицы `orders_items`
--

CREATE TABLE `orders_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order_id` int(11) NOT NULL,
  `catalog_id` int(11) NOT NULL,
  `size_id` int(11) NOT NULL DEFAULT '0',
  `cost` int(11) NOT NULL,
  `count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `orders_items`
--

INSERT INTO `orders_items` (`id`, `created_at`, `updated_at`, `order_id`, `catalog_id`, `size_id`, `cost`, `count`) VALUES
(21, NULL, NULL, 14, 1, 0, 150, 1),
(22, NULL, NULL, 15, 1, 0, 150, 1),
(23, NULL, NULL, 16, 1, 0, 150, 1),
(24, NULL, NULL, 16, 2, 0, 40, 1),
(25, NULL, NULL, 16, 3, 0, 6, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('Test@email.com', '$2y$10$V.ucPrroY/73njNy1pBPAepK92LaVvpiR5joKLb/WyH0/bWsn.D5e', '2018-09-07 05:30:12'),
('Test2@email.com', '$2y$10$GmOAurg/g3ZW3DdPFGqSseELlDySfWiumUdDI4nAxmLLwBOfuFk/O', '2018-09-07 05:30:44'),
('Test3@email.com', '$2y$10$2ihiec1.xmqT3gisqm2QZeZDGeH2FdseqpzOGG2p8KpWoVk.lSQnu', '2018-09-07 07:18:28');

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `created_at`, `updated_at`, `name`, `description`, `image`, `price`, `type`) VALUES
(1, NULL, NULL, 'Рубашка', 'Голубая рубашка', 'product1.jpg', '150.00', 'Мужчина'),
(2, NULL, NULL, 'Туфли', 'Чёрные туфли', 'product2.jpg', '40.00', 'Женщина'),
(3, NULL, NULL, 'Ложка', 'Чайная ложка', 'product3.jpg', '5.80', ''),
(4, NULL, NULL, 'Чашка', 'Кофейная чашка', 'product4.jpg', '9.00', ''),
(7, NULL, NULL, '999', '999', '999.jpg', '999.00', '999');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `admin`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'TestName', 1, 'Test@email.com', '$2y$10$Pre8ODa0T6ZmZ6Q1borCCu9soFqPEXFlpgTbJ6xA7wTifl0DEjQzq', '2Mm373oGZ8UoL4M8Ug1bEUvJTDqfhIN22Al2gW9fR22tPsXBGZ3Oh1K65rqp', '2018-09-04 04:56:08', '2018-09-04 04:56:08'),
(2, 'Test2', 0, 'Test2@email.com', '$2y$10$oHh5bZx4QH1gltyf0Fm6n.hPZZxK03aZQs/M9MXRebnfLW4zTS2ce', 'hEOg1JDFKoBaQ42g995WyMbLmXyWNV2WPo6s4nDc9Xl7GEf8QBW43e3632Wd', '2018-09-07 02:52:33', '2018-09-07 02:52:33'),
(3, 'Test3', 0, 'Test3@email.com', '$2y$10$/SqrdpO2fMXIi6ZbjW5YLuAlp/htiWaYIysm2p1X/BRWeB0qBUJBS', 'WecwKuXdMhSe8TWRYa20X9hjRf5liVSvkc87pQjRQrU3o87BuFjSItAiGnMI', '2018-09-07 02:53:29', '2018-09-07 02:53:29'),
(4, 'test4', 0, 'Test4@email.com', '$2y$10$wkJdpjTs.djQe/9SC6he/erc79BNhBeY0XSdVx9vGPOjzkxhcCyzq', 'oVhpkLN9hMwnRo7XcDosRgmCkh8jHPLeqdWiHV1Yeu3cBXdgEXS3W656UkmI', '2018-09-10 03:50:40', '2018-09-10 03:50:40');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `categories_related`
--
ALTER TABLE `categories_related`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `orders_items`
--
ALTER TABLE `orders_items`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `categories_related`
--
ALTER TABLE `categories_related`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT для таблицы `orders_items`
--
ALTER TABLE `orders_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
